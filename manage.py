#!/usr/bin/env python

from app import create_app, db, services
from flask_script import Manager, Shell
import logging
import os

config = services.get_config()
app = create_app(os.getenv('FLASK_CONFIG') or config)
manager = Manager(app, with_default_commands=False)

# --- SSL
context = None
if app.config['PRIVATE_KEY_FILE'] and app.config['CERTIFICATE_FILE']:
    context = (app.config['CERTIFICATE_FILE'], app.config['PRIVATE_KEY_FILE'])

# --- Logging
if not app.config['DEBUG']:
    handler = logging.StreamHandler()
    handler.setLevel(logging.INFO)
    handler.setFormatter(logging.Formatter("[%(asctime)s] [%(pathname)s:%(lineno)d] - %(levelname)s: %(message)s"))

    app.logger.setLevel(logging.INFO)
    app.logger.addHandler(handler)

# --- Shell
def make_shell_context():
    from app.models import GlobalTag, ScenarioWorkflowSynchMap, GlobalTagMap, GlobalTagMapRequest, IOV, Payload, Record, RecordReleases, ParsedReleases, Tag, O2OErrors, O2OJobs, O2ORun

    return dict(app=app, db=db, GlobalTag=GlobalTag, ScenarioWorkflowSynchMap=ScenarioWorkflowSynchMap,
                GlobalTagMap=GlobalTagMap, GlobalTagMapRequest=GlobalTagMapRequest, IOV=IOV, Payload=Payload,
                Record=Record, RecordReleases=RecordReleases, ParsedReleases=ParsedReleases, Tag=Tag,
                O2OErrors=O2OErrors, O2OJobs=O2OJobs, O2ORun=O2ORun)

manager.add_command("shell", Shell(make_context=make_shell_context))

@manager.command
def generate_keys(coverage=False):
    """ Generates SSL certificates for the local development"""
    """ More info: http://werkzeug.pocoo.org/docs/0.10/serving/#quickstart

        Creates two files in the secrets folder:
        (/data/secrets/keykey.crt, /data/secrets/keykey.key)
    """
    from werkzeug.serving import make_ssl_devcert
    make_ssl_devcert('/data/secrets/key', host=app.config['HOST'])

@manager.command
def test(coverage=False):
    """Runs the unit tests."""
    import unittest
    tests = unittest.TestLoader().discover('tests')
    if not os.getenv('FLASK_CONFIG'):
        return 'Please make sure you set FLASK_CONFIG correctly'
    unittest.TextTestRunner(verbosity=2).run(tests)

@manager.command
def start():
    """Starts the Flask Server."""
    if os.getenv('FLASK_CONFIG'):
        return 'Please make sure you unset FLASK_CONFIG env variable which is only for testing (`unset FLASK_CONFIG`)'

    git_info = 'None'
    if app.config['GIT_INFO']:
        git_info = 'Tag: %s, Commit: %s' % (app.config['GIT_INFO']['tag'], app.config['GIT_INFO']['commit'])

    app.logger.info('%s startup on: %s, Production level: %s, Port: %s, Schema: %s, Git Info: %s',
            app.config['APPLICATION_NAME'], app.config['HOSTNAME'], app.config['PROD_LEVEL'], app.config['PORT'], app.config['SCHEMA_NAME'], git_info)
    app.run(host=app.config['HOST'], port=app.config['PORT'], debug=app.config['DEBUG'], ssl_context=context, threaded=True)

if __name__ == '__main__':
    manager.run()
