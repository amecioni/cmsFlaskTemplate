cmsDbFlaskTemplate
=============

A simple template for CMS DB Flask based services.

Instructions
-----
- Go to: `cd /data/services`
- Create a new dir: `mkdir cmsDbYourName`
- Create a fork of this project
- Clone your fork: `git clone ssh://git@gitlab.cern.ch:7999/YourUserName/cmsFlaskTemplate.git` replace YourUserName accordingly.
- Configure keeper. More info: `https://cms-conddb-dev.cern.ch/docs/creatingANewService.html`
- Add secrets
- Create directory for log files: /data/logs/YourServiceName
- Set up the virtualenv tool/environment:
- `source /data/cmssw/setupEnv.sh`
- `virtualenv --python=python2.7 venv`
- `source venv/bin/activate`
- `pip install -r requirements.txt`
- Run `./manage.py` to list all the possible options
- Run `./manage.py start` or `./run.py` to start the server
- Run `./manage.py shell` to access the interactive python shell with app context
- Run `./manage.py generate_keys` to generates SSL certificates for local developmentt
- To test if everything works smoothly do this:
- `export FLASK_CONFIG='testing'`
- `./manage.py test`
- If you want to start the service through keeper
- you have to deactivate the virtualenv - simply run `deactivate`
- `./keeper.py start cmsDbYourName` (keeper runs venv for you)
