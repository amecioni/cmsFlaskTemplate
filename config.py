import os
from datetime import datetime
from app import services

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

class Config:

    # --- General params
    APPLICATION_NAME = 'cmsDbYourName'
    LOGGER_NAME = 'app'
    HOST = '0.0.0.0'
    SECRETS = services.get_secrets(APPLICATION_NAME)
    SECRET_KEY = SECRETS['encString']
    DB_CONNECTIONS = SECRETS['connections']

    TIME_FORMAT = "%Y-%m-%d %H:%M:%S"
    START_TIME = datetime.now().strftime(TIME_FORMAT)
    GIT_INFO = services.get_git_info()
    HOSTNAME = services.get_hostname()
    PROD_LEVEL = services.get_production_level()
    PORT = services.get_listening_port()

    DEBUG_TB_INTERCEPT_REDIRECTS = False

    ADMIN_GROUP = 'global-tag-administrators'

    # --- Database params
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = True

    SCHEMA_NAME = 'CMS_CONDITIONS'
    SCHEMA_NAME_TESTING = 'CMS_CONDITIONS_TEST'

    prep_secrets = SECRETS['connections']['Prep']
    prod_secrets = SECRETS['connections']['Prod']
    prep_db = 'oracle://%s:%s@%s' % (prep_secrets['user'], prep_secrets['password'], prep_secrets['db_name'])
    prod_db = 'oracle://%s:%s@%s' % (prod_secrets['user'], prod_secrets['password'], prod_secrets['db_name'])

    SQLALCHEMY_BINDS = {
        'Prep': prep_db,
        'Prod': prod_db
    }

    # --- SSL params
    PRIVATE_KEY_FILE = None
    CERTIFICATE_FILE = None

    if PROD_LEVEL == 'private':
        if os.path.isfile('/data/secrets/key.key'):
            PRIVATE_KEY_FILE = '/data/secrets/key.key'

        if os.path.isfile('/data/secrets/key.crt'):
            CERTIFICATE_FILE = '/data/secrets/key.crt'
    else:
        if os.path.isfile('/data/secrets/hostkey.pem'):
            PRIVATE_KEY_FILE = '/data/secrets/hostkey.pem'

        if os.path.isfile('/data/secrets/hostcert.pem'):
            CERTIFICATE_FILE = '/data/secrets/hostcert.pem'

    # --- Mail params
    MAIL_SERVER = 'smtp.cern.ch'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = SECRETS['email']['username']
    MAIL_PASSWORD = SECRETS['email']['password']
    MAIL_SUBJECT_PREFIX = '[%s]' % APPLICATION_NAME
    MAIL_SENDER = MAIL_USERNAME + '@cern.ch'
    MAIL_ADMIN = 'global-tag-administrators@cern.ch'

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    DB_TYPE = 'Prep'


class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    DB_TYPE = 'Prep'
    SCHEMA_NAME = 'CMS_CONDITIONS_TEST'


class ProductionConfig(Config):
    DEBUG = False
    DB_TYPE = 'Prod'


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,

    'default': DevelopmentConfig
}
