#!/usr/bin/env python

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from flask_httpauth import HTTPBasicAuth
from flask_mail import Mail
from flask_sslify import SSLify
from flask_login import LoginManager
from .db_alchemy import MultiDBSQLAlchemy
from config import config

db = MultiDBSQLAlchemy()
bootstrap = Bootstrap()
mail = Mail()
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'

def create_app(config_name):
    conf = config[config_name]
    url_prefix = '/%s' % conf.APPLICATION_NAME
    app = Flask(__name__, static_url_path=url_prefix+'/static')
    app.config.from_object(conf)
    conf.init_app(app)
    app.app_context().push()

    login_manager.init_app(app)
    mail.init_app(app)
    db.init_app(app)
    bootstrap.init_app(app)
    sslify = SSLify(app)

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix=url_prefix+'/auth')

    from .db_layer import db_layer as db_layer_blueprint
    app.register_blueprint(db_layer_blueprint, url_prefix=url_prefix+'/db')

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint, url_prefix=url_prefix)

    return app
