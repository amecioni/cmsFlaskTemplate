""" File handles all db related requests from the client side """

from flask import Response, render_template, session, redirect, url_for, current_app, flash, jsonify, g, request
from flask_login import login_user, logout_user, login_required, current_user
from . import db_layer, querying
from .. import db
from ..utils import utils
from ..email import send_email
import json
from datetime import datetime

@db_layer.route('/get_gt_tags', methods=["GET"])
def get_gt_tags():
    db_param = request.values.get("database")
    gt = request.values.get("gt")
    return json.dumps({"data": querying.get_gt_tags(db_param, gt)})
