from flask import Flask, g, current_app
from flask_sqlalchemy import SQLAlchemy

class MultiDBSQLAlchemy(SQLAlchemy):

    # Wrapper class which adds some hacks for Flask-SQLAlchemy extension
    # that allows to connect to different databases based on the request parameters

    def choose_db(self, bind_key):
        if hasattr(g, 'db_bind_key'):
            if g.db_bind_key != bind_key:
                raise RuntimeError('Switching db in the middle of the request.')
        g.db_bind_key = bind_key

    def get_engine(self, app=None, bind=None):
        if bind is None:
            if not hasattr(g, 'db_bind_key'):
                raise RuntimeError('No db chosen.')
            bind = g.db_bind_key
        return super(self.__class__, self).get_engine(app=app, bind=bind)

    def apply_driver_hacks(self, app, info, options):
        # Overrides Flask-SQLAlchemy extensions apply_driver_hacks function
        # and adds label_length option which solves a label bug (it becomes too big for some queries and it fails to execute)
        # label_length: http://docs.sqlalchemy.org/en/latest/core/engines.html#sqlalchemy.create_engine.params.label_length
        options['label_length'] = 5
        return super(self.__class__, self).apply_driver_hacks(app=app, info=info, options=options)
