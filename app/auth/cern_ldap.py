""" Common code for searching in the CERN LDAP server. """

import logging
import ldap

# TODO: It seems the LDAP connection times out, re-connect automatically if so.

class CERNLDAPError(Exception):
    """ A common CERNLDAP exception. """

class NotFoundError(CERNLDAPError):
    """ Not found error (e.g. username not found). """

class CERNLDAP(object):
    """ Class used for searching in the CERN LDAP server. """

    def __init__(self, server = 'ldap://xldap.cern.ch'):
        self.server = server
        self.ldap = ldap.initialize(self.server)

    def __str__(self):
        return 'CERNLDAP %s' % self.server

    def get_user_info(self, username, attributes = None, timeout = None):
        if timeout is None:
            timeout = -1

        result = self.ldap.search_st('ou=users,ou=organic units,dc=cern,dc=ch', ldap.SCOPE_SUBTREE, '(cn=%s)' % username, attributes, timeout = timeout)
        if len(result) == 0:
            raise NotFoundError('%s: Username "%s" not found.' % (self, username))

        return result[0][1]

    def get_username_by_id(self, id, attributes = None, timeout = None):
        if timeout is None:
            timeout = -1

        result = self.ldap.search_st('ou=users,ou=organic units,dc=cern,dc=ch', ldap.SCOPE_SUBTREE, '(employeeID=%s)' % id, attributes, timeout = timeout)
        if len(result) == 0:
            raise NotFoundError('%s: User id "%s" not found.' % (self, id))

        return '%s (%s)' % (result[0][1]['displayName'][0], result[0][1]['cn'][0])

    def get_user_email(self, username, timeout = None):
        return self.get_user_info(username, ['mail'], timeout)['mail'][0]
