from functools import wraps
from flask import abort, current_app, flash, session

def validate_db(f):
    @wraps(f)
    def decorated_function(db, *args, **kwargs):
        if db not in current_app.config['SQLALCHEMY_BINDS']:
            flash('Please check if database parameter in the URL is set correctly', 'danger')
            abort(404)
        session['db'] = db
        return f(db, *args, **kwargs)
    return decorated_function
