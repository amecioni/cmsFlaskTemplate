from flask import Flask, jsonify
from datetime import datetime
from ..services import get_config
from config import config
from webargs import ValidationError

conf_file = get_config()
conf = config[conf_file]

def timestamp():
    return datetime.utcnow()

def success():
    return jsonify({'success': True, 'status': 200})
