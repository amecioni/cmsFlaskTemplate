from flask import current_app
from . import db, services
from .utils import utils
from datetime import datetime
from sqlalchemy.types import TIMESTAMP
import json

SCHEMA = current_app.config['SCHEMA_NAME']

def create_fk(column, **kwargs):
    column = "%s.%s" % (SCHEMA, column)
    return db.ForeignKey(column, **kwargs)

class GlobalTag(db.Model):
    __tablename__ = 'GLOBAL_TAG'
    __table_args__ = {'schema' : SCHEMA}

    name = db.Column(db.String(100), unique=True, nullable=False, primary_key=True)
    validity = db.Column(db.Integer, nullable=False)
    description = db.Column(db.String(4000), nullable=False)
    release = db.Column(db.String(100), nullable=False)
    insertion_time = db.Column(db.DateTime, nullable=False)
    snapshot_time = db.Column(db.DateTime, nullable=False)
    scenario = db.Column(db.String(100))
    workflow = db.Column(db.String(100))
    type = db.Column(db.String(1))
    tags = db.relationship('GlobalTagMap', backref='global_tag', lazy='dynamic')

    def __repr__(self):
        return '<GlobalTag %r>' % self.name


class ScenarioWorkflowSynchMap(db.Model):
    __tablename__ = 'SCENARIO_WORKFLOW_SYNCH_MAP'
    __table_args__ = {'schema' : SCHEMA}

    scenario = db.Column(db.String(100), primary_key=True, nullable=False)
    workflow = db.Column(db.String(100), primary_key=True, nullable=False)
    synchronization = db.Column(db.String(4000), primary_key=True, nullable=False)


class GlobalTagMap(db.Model):
    __tablename__ = 'GLOBAL_TAG_MAP'
    __table_args__ = {'schema' : SCHEMA}

    global_tag_name = db.Column(db.String(100), create_fk('GLOBAL_TAG.name'), primary_key=True, nullable=False)
    record = db.Column(db.String(100), create_fk('RECORDS.record'), primary_key=True, nullable=False)
    label = db.Column(db.String(100), primary_key=True, nullable=False)
    tag_name = db.Column(db.String(100), create_fk('TAG.name'), nullable=False)

    def __repr__(self):
        return '<GlobalTagMap %r>' % self.global_tag_name

    def to_array(self):
        return [self.record, self.label, self.tag_name]

    @staticmethod
    def to_datatables(tags):
        data = {
            'headers': ['Record', 'Label', 'Tag'],
            'data': [ t.to_array() for t in tags ],
        }
        return data

class GlobalTagMapRequest(db.Model):
    __tablename__ = 'GLOBAL_TAG_MAP_REQUEST'
    __table_args__ = {'schema' : SCHEMA}

    queue = db.Column(db.String(100), primary_key=True, nullable=False)
    tag = db.Column(db.String(100), create_fk('TAG.name'), primary_key=True, nullable=False)
    record = db.Column(db.String(100), create_fk('RECORDS.record'), primary_key=True, nullable=False)
    label = db.Column(db.String(100), primary_key=True, nullable=False)
    status = db.Column(db.String(1), nullable=False)
    description = db.Column(db.String(4000), nullable=False)
    submitter_id = db.Column(db.Integer, nullable=False)
    time_submitted = db.Column(db.DateTime, nullable=False)
    last_edited = db.Column(db.DateTime, nullable=False)

    def __repr__(self):
        return '<GlobalTagMapRequest %r>' % self.time_submitted


class IOV(db.Model):
    __tablename__ = 'IOV'
    __table_args__ = {'schema' : SCHEMA}

    tag_name = db.Column(db.String(4000), create_fk('TAG.name'), primary_key=True, nullable=False)
    since = db.Column(db.Integer, primary_key=True, nullable=False)
    payload_hash = db.Column(db.String(40), create_fk('PAYLOAD.hash'), nullable=False)
    insertion_time = db.Column(db.DateTime, primary_key=True, nullable=False)

    def __repr__(self):
        return '<IOV %r>' % self.tag_name


class Payload(db.Model):
    __tablename__ = 'PAYLOAD'
    __table_args__ = {'schema' : SCHEMA}

    hash = db.Column(db.String(40), primary_key=True, nullable=False)
    object_type = db.Column(db.String(4000), nullable=False)
    version = db.Column(db.String(4000), nullable=False)
    insertion_time = db.Column(db.DateTime, nullable=False)

    def __repr__(self):
        return '<Payload %r>' % self.hash


class Record(db.Model):
    __tablename__ = 'RECORDS'
    __table_args__ = {'schema' : SCHEMA}

    record = db.Column(db.String(100), primary_key=True, nullable=False)
    object = db.Column(db.String(200), nullable=False)
    type = db.Column(db.String(20), nullable=False)

    def __repr__(self):
        return '<Record %r>' % self.record


class RecordReleases(db.Model):
    __tablename__ = 'RECORD_RELEASES'
    __table_args__ = {'schema' : SCHEMA}

    record = db.Column(db.String(100), create_fk('RECORDS.record'), primary_key=True, nullable=False)
    release_cycle = db.Column(db.String(100), primary_key=True, nullable=False)
    release = db.Column(db.String(100), nullable=False)
    release_int = db.Column(db.String(100), nullable=False)

    def __repr__(self):
        return '<RecordReleases %r>' % self.record


class ParsedReleases(db.Model):
    __tablename__ = 'PARSED_RELEASES'
    __table_args__ = {'schema' : SCHEMA}

    release_cycle = db.Column(db.String(100), primary_key=True, nullable=False)
    release = db.Column(db.String(100), nullable=False)
    release_int = db.Column(db.String(100), nullable=False)

    def __repr__(self):
        return '<ParsedReleases %r>' % self.release_cycle


class Tag(db.Model):
    __tablename__ = 'TAG'
    __table_args__ = {'schema' : SCHEMA}

    name = db.Column(db.String(4000), primary_key=True, nullable=False)
    time_type = db.Column(db.String(4000), nullable=False)
    object_type = db.Column(db.String(4000), nullable=False)
    synchronization = db.Column(db.String(4000), nullable=False)
    end_of_validity = db.Column(db.Integer, nullable=False)
    description = db.Column(db.String(4000), nullable=False)
    last_validated_time = db.Column(db.Integer, nullable=False)
    insertion_time = db.Column(db.DateTime, nullable=False)
    modification_time = db.Column(db.DateTime, nullable=False)

    def __repr__(self):
        return '<Tag %r>' % self.name

class TagLog(db.Model):
    __tablename__ = 'TAG_LOG'
    __table_args__ = {'schema' : SCHEMA}

    tag_name = db.Column(db.String(100), create_fk('TAG.name'), primary_key=True, nullable=False)
    event_time = db.Column(db.DateTime, primary_key=True, nullable=False)
    user_name = db.Column(db.String(100), nullable=False)
    host_name = db.Column(db.String(100), nullable=False)
    command = db.Column(db.String(500), nullable=False)
    action = db.Column(db.String(100), primary_key=True, nullable=False)
    user_text = db.Column(db.String(4000), nullable=False)

    def __repr__(self):
        return '<TagLog %r>' % self.tag_name

    def to_array(self):
        return [self.tag_name, utils.to_timestamp(self.event_time), self.user_name, self.host_name, self.command, self.action, self.user_text]

    @staticmethod
    def to_datatables(tag_logs):
        logs_data = {
            'headers': ["Tag Name", "Event Time", "User", "Host", "Command", "Action", "User Text"],
            'data': [ l.to_array() for l in tag_logs ],
        }
        return logs_data

class O2OErrors(db.Model):
    __tablename__ = 'O2O_ERROR_ACK'
    __table_args__ = {'schema' : SCHEMA}

    job_name = db.Column(db.String(100), create_fk('O2O_RUN.job_name'), primary_key=True, nullable=False)
    start_time = db.Column(TIMESTAMP, create_fk('O2O_RUN.start_time'), primary_key=True, nullable=False)
    insertion_time = db.Column(db.DateTime, nullable=False)
    rationale = db.Column(db.String(1000), nullable=False)
    username = db.Column(db.String(30), nullable=False)

    def __repr__(self):
        return '<O2OErrors %r>' % self.job_name


class O2OJobs(db.Model):
    __tablename__ = 'O2O_JOB'
    __table_args__ = {'schema' : SCHEMA}

    name = db.Column(db.String(100), primary_key=True, nullable=False)
    interval = db.Column(db.Integer, nullable=False)
    tag_name = db.Column(db.String(100), create_fk('TAG.name'), nullable=False)
    enabled = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<O2OJobs %r>' % self.name


class O2ORun(db.Model):
    __tablename__ = 'O2O_RUN'
    __table_args__ = {'schema' : SCHEMA}

    job_name = db.Column(db.String(100), primary_key=True, nullable=False)
    start_time = db.Column(db.DateTime, primary_key=True, nullable=False)
    end_time = db.Column(db.DateTime, nullable=True)
    status_code = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<O2ORun %r>' % self.job_name
