from flask import render_template, session, abort, redirect, url_for, current_app, flash, jsonify, g, request
from flask_login import login_user, logout_user, login_required, current_user
from . import main
from .. import db, login_manager
from ..utils import utils
from ..utils.decorators import validate_db
from ..auth.decorators import admin_required
from ..email import send_email
from ..db_layer import querying
import json
import datetime

@main.before_request
def before_request():
    if 'db' not in session:
        session['db'] = current_app.config['DB_TYPE']

@main.route('/index/<string:db>')
@validate_db
def index_db(db):
    return render_template('index.html')

@main.route('/')
@main.route('/index')
def index():
    return redirect(url_for('main.index_db', db=session['db']))

@main.route('/test/<string:db>')
@validate_db
def test(db):
    data = querying.get_gt_tags(db, '74X_dataRun1_v1')
    current_app.logger.info(len(data))
    return render_template('test.html', data=data)

@main.route('/test_admin/')
@admin_required
def test_admin():
    return render_template('test_admin.html')
